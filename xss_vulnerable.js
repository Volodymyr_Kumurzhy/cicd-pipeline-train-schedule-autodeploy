var http = require('http');
var url = require('url');

http.createServer(function (req, res) {
    var queryData = url.parse(req.url, true).query;
    res.writeHead(200, {"Content-Type": "text/html"});
    res.end('<h1>' + queryData.name + '</h1>');
}).listen(8080);