import pickle

def insecure_deserialization(payload):
    return pickle.loads(payload)

def test_vulnerable_function():
    payload = b'\x80\x03}q\x00.'
    result = insecure_deserialization(payload)
    print(result)

test_vulnerable_function()
